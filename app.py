import json
import random
import string

from flask import Flask, request, make_response, jsonify, redirect

app = Flask(__name__)


@app.route("/")
def main():
    return jsonify({"status": "OK"})


@app.route("/redirect-to-headers")
def get_headers_redirect():
    return redirect('/headers')


@app.route('/headers')
def get_headers():
    response = make_response(
        json.dumps(dict(request.headers), indent=2)
    )
    response.headers['Content-Type'] = 'text/plain'
    return response


@app.route('/random')
def get_random():
    length = request.args.get('length', '10')

    try:
        length = int(length)
    except ValueError:
        return "Error. Length is not integer", 400

    length = int(length)

    chars = string.ascii_lowercase + string.ascii_uppercase

    result = []
    for i in range(length):
        result.append(random.choice(chars))

    return "".join(result)


app.run(debug=True)
