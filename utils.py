import os


filename = os.path.join(
    os.path.dirname(__file__),
    'requirements.txt'
)

print(filename)

with open(filename, 'r', encoding='utf-8') as f:
    print(f.read())
